﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoiceBase_API
{
    class Models
    {


    }


    public class Rootobject
    {
        public Keywords keywords { get; set; }
        public Topics topics { get; set; }
    }

    public class Keywords
    {
        public LatestKey latest { get; set; }
    }

    public class LatestKey
    {
        public string revision { get; set; }
        public Word[] words { get; set; }
        public object[] groups { get; set; }
    }

    public class Word
    {
        public T t { get; set; }
        public string name { get; set; }
        public float relevance { get; set; }
    }

    public class T
    {
        public string[] Agent { get; set; }
        public string[] Caller { get; set; }
    }

    public class Topics
    {
        public LatestTop latest { get; set; }
    }

    public class LatestTop
    {
        public string revision { get; set; }
        public Topic[] topics { get; set; }
    }

    public class Topic
    {
        public float score { get; set; }
        public Keyword[] keywords { get; set; }
        public string[] speakers { get; set; }
        public string name { get; set; }
        public object[] similarCategories { get; set; }
        public object[] subcategories { get; set; }
        public string type { get; set; }
    }

    public class Keyword
    {
        public string[] internalName { get; set; }
        public float score { get; set; }
        public T1 t { get; set; }
        public string name { get; set; }
    }

    public class T1
    {
        public string[] Caller { get; set; }
        public string[] Agent { get; set; }
    }


}
