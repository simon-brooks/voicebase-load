﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using System.Data.SqlClient;
using System.IO;
using System.Web.Script.Serialization;
using System.Net;
using System.Threading;
using System.Linq;
using Npgsql;
using ExcelDataReader;
using System.Data;
using Microsoft.VisualBasic.FileIO;
using System.Web;

namespace VoiceBase_API
{

    public class Authorisation
    {
        public string token { get; set; }
        public string success { get; set; }
        public string message { get; set; }
    }

    public class MediaResponse
    {
        public string mediaId { get; set; }
        public string status { get; set; }
        public Keywords keywords { get; set; }
        public Topics topics { get; set; }

        //       public string message { get; set; }
    }

    public class Media
    {
//        public string media { get; set; }
        public MediaResponse media { get; set; }
    }

    public class Vocabs
    {
        public string name;
        public string[] terms;
    }


    public class Terms
    {
        public string[] soundsLike;
        public string term;
        public int? weight;
    }

    public class VocabsV3
    {
        public string vocabularyName;
        public Terms[] terms;
    }



    public class VocabContainer
    {
        public Vocabs vocabulary;

    }

    public class VocabContainerV3
    {
        public VocabsV3 vocabulary;

    }


    public class KeywordsSet
    {
        public string name;
        public string[] keywords;
    }

    public class KeywordGroup
    {
        public Keywords group;

    }

    public class datacom_meta
    {
        public string recording_id;
        public string date_time;
        public string recording_length;
        public string direction;
        public string initiator_address;
        public string interaction_address;
        public string agent;
        public string scoring_user;
        public string scoring_status;
        public string queue;
        public string interaction_id;
        public string initiating_policy;
    }

    public class iSelect_meta
    {
        public string cli;
        public string agent;
        public DateTime date_time;
        public string filename;
        public string info3;
        public Double length;
        public string queue;
    }

    public static class config
    {
        public const string dbConn = "Server=pg-daisee-ekko-test.cquvuslt1stb.ap-southeast-2.rds.amazonaws.com;Port=5432;User Id=ekko_app_user;Password=Pa$$word0123;Database=lisa_king;";
        public const string trans_config_id = "7a4f6b3e-7361-11e8-a5d6-e37afc3154ea";
    }



    public class Program
    {
        static string token;

        static void Main()
        {


            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            // ** Set SQL connection

            NpgsqlConnection ekkoDbConnection = new NpgsqlConnection(config.dbConn);
            ekkoDbConnection.Open();

            // Set directory variables

            string DirDate = @"kingprice\uploads\SALES Recordings";
            //            int noToProcess = Int32.Parse(args[1]);
            int noToProcess = 5000;
 

            string BaseMediaDir = @"Z:\client_data\"+DirDate;
            string BaseInputDir = "\\\\efs\\newsltdshare1\\News POC June 2016\\Output Files";
            string outdir = BaseMediaDir + "\\TXT";
            string JSONdir = BaseMediaDir + "\\JSON";

            //** Get and set Token

            token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJjOTk5Nzc2Ny1lMGM4LTRiOGMtOGJmYi00YjNkNTc5MjJiYjkiLCJ1c2VySWQiOiJhdXRoMHw1Yjk5OTQ3Mjk3ZWRiNTIyNWJmMzIxMTgiLCJvcmdhbml6YXRpb25JZCI6IjY2YzE1NWI4LTYyNDAtNDViMC1iM2JkLWNkMmE4MjM5NDgzNSIsImVwaGVtZXJhbCI6ZmFsc2UsImlhdCI6MTUzNjc5MTc2Mzk3MiwiaXNzIjoiaHR0cDovL3d3dy52b2ljZWJhc2UuY29tIn0.wwrl1Mu1B-0OSnSBy_xOLYRD3s4PCzrh11AaDpVuYuU";
            // ** Set Custom Vocabs

            string cusVocab = "king";

            //           RemoveRefId(ekkoDbConnection, @"Z:\client_data\Datacom\IPA\MayData\Total May 2018\duplicates.csv", @"Z:\client_data\Datacom\IPA\MayData\Total May 2018\JSON\");



            //SetVocab(ekkoDbConnection, cusVocab);
            SetVocabV3(ekkoDbConnection, cusVocab);

            //List<datacom_meta> dmeta = Load_Datacom_meta(BaseMediaDir);
            //List<iSelect_meta> dmeta = Load_iSelect_meta(BaseMediaDir);

            // ** Send all files in directory to VoiceBase

            //CopyMP3Files(BaseMediaDir, BaseMediaDir + @"\MP3");

            SendToVoiceBase(ekkoDbConnection, BaseInputDir, BaseMediaDir, DirDate, noToProcess, cusVocab);

            // ** Update status of non processed files

            //                        GetOutputFromVoiceBase(ekkoDbConnection, outdir);
//            LoadKeywordTopic(ekkoDbConnection, JSONdir);
//            GetPlainTextVoiceBase(ekkoDbConnection, outdir);
            //           countFiles(BaseMediaDir);

            //           RemoveDuplicate(myConnection, BaseMediaDir + "\\JSON");
            //ReSendToVoiceBase(ekkoDbConnection, @"Z:\client_data\iSelect\POC_June2018\isel_recordings_daisee", cusVocab, dmeta);

            //          CreateOutputFiles(myConnection, BaseMediaDir + "\\JSON");

            //           SetKeywordGroup();

            ekkoDbConnection.Close();
        }


        public static void LoadKeywordTopic(NpgsqlConnection myConnection, string JSONdir)
        {
            Guid trans_id;
            string json = "";
            Guid topic_id;

            foreach (string file in Directory.GetFiles(JSONdir))
            {
                json = File.ReadAllText(file);
                json = json.Replace("\"Agent \":", "\"Agent\":");
                json = json.Replace("\"Caller \":", "\"Caller\":");

                var json_serializer = new JavaScriptSerializer();
                Media VBJson = json_serializer.Deserialize<Media>(json);
                trans_id = GetTranscriptId(myConnection, VBJson.media.mediaId);

                //keywords
                
                foreach (Word w in VBJson.media.keywords.latest.words)
                {
                    NpgsqlCommand insert = new NpgsqlCommand();
                    try
                    {
                        insert.Connection = myConnection;
                        insert.CommandText = "INSERT INTO public.trans_keywords " +
                            "(id, transcript_id, name, relevence, agent_count, agent_times, client_count, client_times) " +
                            "VALUES " +
                            "(@id, @transcript_id, @name, @relevence, @agent_count, @agent_times, @client_count, @client_times) ";
                        insert.Parameters.AddWithValue("@id", Guid.NewGuid());
                        insert.Parameters.AddWithValue("@transcript_id", trans_id);
                        insert.Parameters.AddWithValue("@name", w.name);
                        insert.Parameters.AddWithValue("@relevence", w.relevance);
                        int agent_count = 0;
                        string agent_times = "";
                        if (w.t.Agent != null)
                        {
                            agent_count = w.t.Agent.Length;
                            agent_times = string.Join(",", w.t.Agent);
                        }
                        int client_count = 0;
                        string client_times = "";
                        if (w.t.Caller != null)
                        {
                            client_count = w.t.Caller.Length;
                            client_times = string.Join(",", w.t.Caller);
                        }

                        insert.Parameters.AddWithValue("@agent_count", agent_count);
                        insert.Parameters.AddWithValue("@agent_times", agent_times);
                        insert.Parameters.AddWithValue("@client_count", client_count);
                        insert.Parameters.AddWithValue("@client_times", client_times);


                        insert.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                    }





                }
                
                //topics

                foreach (Topic t in VBJson.media.topics.latest.topics)
                {
                    topic_id = Guid.NewGuid();
                    NpgsqlCommand insert = new NpgsqlCommand();
                    try
                    {
                        insert.Connection = myConnection;
                        insert.CommandText = "INSERT INTO public.trans_topic " +
                            "(id, transcript_id, topic_name, score, agent, caller) " +
                            "VALUES " +
                            "(@id, @transcript_id, @topic_name, @score, @agent, @caller) ";
                        insert.Parameters.AddWithValue("@id", topic_id);
                        insert.Parameters.AddWithValue("@transcript_id", trans_id);
                        insert.Parameters.AddWithValue("@topic_name", t.name);
                        insert.Parameters.AddWithValue("@score", t.score);
                        insert.Parameters.AddWithValue("@agent", t.speakers.Contains("Agent "));
                        insert.Parameters.AddWithValue("@caller", t.speakers.Contains("Caller "));
                        insert.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                    }



                    foreach (Keyword k in t.keywords)
                    {
  //                      NpgsqlCommand insertk = new NpgsqlCommand();
                        try
                        {
  //                          insertk.Connection = myConnection;
                            insert.CommandText = "INSERT INTO public.trans_topic_keywords " +
                                "(id, trans_topic_id, keyword_name, score, agent_count, agent_times, client_count, client_times) " +
                                "VALUES " +
                                "(@id, @trans_topic_id, @keyword_name, @score, @agent_count, @agent_times, @client_count, @client_times) ";
                            insert.Parameters.Clear();
                            insert.Parameters.AddWithValue("@id", Guid.NewGuid());
                            insert.Parameters.AddWithValue("@trans_topic_id", topic_id);
                            insert.Parameters.AddWithValue("@keyword_name", k.name);
                            insert.Parameters.AddWithValue("@score", k.score);
                            int agent_count = 0;
                            string agent_times = "";
                            if (k.t.Agent != null)
                            {
                                agent_count = k.t.Agent.Length;
                                agent_times = string.Join(",", k.t.Agent);
                            }
                            int client_count = 0;
                            string client_times = "";
                            if (k.t.Caller != null)
                            {
                                client_count = k.t.Caller.Length;
                                client_times = string.Join(",", k.t.Caller);
                            }
                            insert.Parameters.AddWithValue("@agent_count", agent_count);
                            insert.Parameters.AddWithValue("@agent_times", agent_times);
                            insert.Parameters.AddWithValue("@client_count", client_count);
                            insert.Parameters.AddWithValue("@client_times", client_times);
                            insert.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {

                        }

                    }




                }

            }
        }

        static Guid GetTranscriptId(NpgsqlConnection ekkoDbConnection, string ceres_id)
        {
            NpgsqlCommand sql = new NpgsqlCommand("select id " +
                                                    "from transcript t " +
                                                    "where t.ref_id = '" + ceres_id + "' ", ekkoDbConnection);
            try
            {
                return (Guid)sql.ExecuteScalar();
            }
            catch (Exception e)
            {
                return Guid.Empty;
            }
        }



        public static List<datacom_meta> Load_Datacom_meta(string filelocation)
        {
            List<datacom_meta> dmeta = new List<datacom_meta>();

            string filePath = filelocation + "\\May 2018 data.xls";
            int irow = 0;

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {

                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {

                    // 1. Use the reader methods
                    while (reader.Read())
                    {
                        irow++;
                        if (irow == 1)  // skip header row
                        {
                            continue;
                        }
                        datacom_meta drow = new datacom_meta();
                        drow.recording_id = reader.GetString(0);
                        drow.date_time = reader.GetString(1);
                        drow.recording_length = reader.GetString(2);
                        drow.direction = reader.GetString(3);
                        drow.initiator_address = reader.GetString(4);
                        drow.interaction_address = reader.GetString(5);
                        drow.agent = reader.GetString(6);
                        drow.scoring_user = reader.GetString(7);
                        drow.scoring_status = reader.GetString(8);
                        drow.queue = reader.GetString(9);
                        drow.interaction_id = reader.GetString(10);
                        drow.initiating_policy = reader.GetString(11);
                        dmeta.Add(drow);
                    }
                }
            }



            return dmeta;
        }


        public static List<iSelect_meta> Load_iSelect_meta(string filelocation)
        {
            List<iSelect_meta> dmeta = new List<iSelect_meta>();

            string filePath = filelocation + "\\iselect_recordings_metadata.xlsx";
            int irow = 0;

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {

                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {

                    // 1. Use the reader methods
                    while (reader.Read())
                    {
                        irow++;
                        if (irow == 1)  // skip header row
                        {
                            continue;
                        }
                        iSelect_meta drow = new iSelect_meta();
                        drow.cli = reader.GetString(0);
                        drow.agent = reader.GetString(1);
                        drow.date_time = reader.GetDateTime(2);
                        drow.filename = reader.GetString(3);
                        drow.info3 = reader.GetString(4);
                        drow.length = reader.GetDouble(5);
                        drow.queue = reader.GetString(6);
                        dmeta.Add(drow);
                    }
                }
            }



            return dmeta;
        }

        static void RemoveRefId(NpgsqlConnection myConnection, string csvfile, string JSONdir)
        {
            // Temporary function to remove duplicate records from database based on list of media Id's in csv file
            NpgsqlConnection delconn = new NpgsqlConnection(config.dbConn);
            delconn.Open();


            using (TextFieldParser csvParser = new TextFieldParser(csvfile))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                // Skip the row with the column names
                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();
                    string transcript_id = fields[0];

                    // get dm_recording_file_id and dm_recording_interaction_id

                    NpgsqlCommand sql = new NpgsqlCommand("SELECT rf.id as rfId, rf.recording_interaction_id as riId from  " +
                                                            " transcript t " +
                                                            " join dm_recording_file rf on (t.recording_file_id = rf.id) " +
                                                            " where t.ref_id = '" + transcript_id + "' ", myConnection);
                    try
                    {
                        using (NpgsqlDataReader reader = sql.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    int rfId = reader.GetInt32(reader.GetOrdinal("rfId"));
                                    int riId = reader.GetInt32(reader.GetOrdinal("riId"));

                                    // Remove transcript record

                                    NpgsqlCommand sqldel = new NpgsqlCommand("delete from transcript where recording_file_id = " + rfId.ToString(), delconn);
                                    sqldel.ExecuteNonQuery();

                                    //Remove recording file record
                                    sqldel.CommandText = "delete from dm_recording_file where id = " + rfId.ToString();
                                    sqldel.ExecuteNonQuery();

                                    //Remove recording interaction record
                                    sqldel.CommandText = "delete from dm_recording_interaction where recording_interaction_id = " + riId.ToString();
                                    sqldel.ExecuteNonQuery();

                                    File.Delete(Path.Combine(JSONdir, transcript_id + ".json"));

                                }
                            }
                        }

                        continue;
                    }
                    catch (Exception e)
                    {
                        //skip
                        continue;


                    }

                }
            }
            delconn.Close();

        }



        public void Main(string v1, string v2)
        {

            throw new NotImplementedException();
        }

        static void RemoveDuplicate(SqlConnection myConnection, string outDir)
        {
            string[] fileEntries = Directory.GetFiles(Path.Combine(outDir));
            foreach (string file in fileEntries)
            {
                string mediaId = Path.GetFileNameWithoutExtension(file);
                SqlCommand cmdSelect = new SqlCommand("select mediaId from ekko where mediaId = '" + mediaId + "'", myConnection);

                using (SqlDataReader reader = cmdSelect.ExecuteReader())
                {
                    if (reader.HasRows)
                    {

                    }
                    else
                    {
                        File.Delete(file);
                    }

                }
            }
        }

        static bool CreateOutputFiles(SqlConnection myConnection, string outDir)
        {
            SqlCommand cmdSelect = new SqlCommand("select id, filename, Text_Plain, JSON from ekko where id < 41 order by id asc", myConnection);

            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string Text_Plain = reader.GetString(reader.GetOrdinal("Text_Plain"));
                        string json = reader.GetString(reader.GetOrdinal("JSON"));
                        string filename = reader.GetString(reader.GetOrdinal("filename"));
                        string outfiletxt = filename.Replace(".mp3", ".txt");
                        string outfilejson = filename.Replace(".mp3", ".json");
                        outfilejson = outfilejson.Replace(".wav", ".json");
                        outfilejson = outfilejson.Replace(".WAV", ".json");
                        outfilejson = outfilejson.Replace(".MP3", ".json");
                        //                       outfile = outfile.Replace("%20", " ");
                        string destFile = System.IO.Path.Combine(outDir, outfiletxt);
 //                       File.WriteAllText(destFile, Text_Plain);
                        destFile = System.IO.Path.Combine(outDir, outfilejson);
                        File.WriteAllText(destFile, json);
                    }
                }
            }
            return true;
        }


        static string GetToken()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v2-beta/");
            var request = new RestRequest();

            //  ****  News Ltd POC Account
            //            request.AddParameter("apikey", "4CDe7C07E6783FafB89C07eFC35-C641423D4ddD5D64652487175e1C83FAb4F858FBB03C7cC-32D570E9E9");
            //            request.AddParameter("password", "448StKilda");

            //  *** Torquex Account
                        request.AddParameter("apikey", "0c9E44ef5DC342DfF8b8B878BeA-BDF07E6424399Dd193601909F3fa689EEE1DCE112740E80-13EFF53fDA");
                        request.AddParameter("password", "Jasper123");

            IRestResponse<Authorisation> response = client.Execute<Authorisation>(request);
            return response.Data.token;

        }


        static string GetMediaV3(string file, string cusVocab, NpgsqlConnection conf, string fraud, string yourCallId)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v3");
            var request = new RestRequest(Method.POST);
            request.Resource = "/media";
            request.AddHeader("Authorization", "Bearer " + token);
            //            request.AddHeader("Content-Type", "multipart/form-data");
            string fileName = Path.GetFileName(file);
            File.Copy(file, Path.Combine(@"C:\Temp", fileName));
            string file2 = Path.Combine(@"C:\Temp", fileName);
            request.Timeout = 3000000;
            string configuration_string;
            string metadata_string;
            string vocabs;
            string language;
            int channels = 1;
            string diarize = "";
            string left_speaker = "";
            string right_speaker = "";

            NpgsqlCommand sql = new NpgsqlCommand("select language, channels, left_channel_speaker, right_channel_speaker from cfg_transcript where id = '" + config.trans_config_id + "'", conf);

            using (var reader = sql.ExecuteReader())
            {
                if (!reader.Read())
                    throw new Exception("Something is wrong");
                language = reader.GetString(reader.GetOrdinal("language"));
                channels = reader.GetInt32(reader.GetOrdinal("channels"));
                left_speaker = reader.GetString(reader.GetOrdinal("left_channel_speaker"));
                right_speaker = reader.GetString(reader.GetOrdinal("right_channel_speaker"));
            }

            if (channels == 2)
            {
                diarize = "\"stereo\": {\"left\": {\"speakerName\": \"" + left_speaker + " \" },\"right\": {\"speakerName\": \"" + right_speaker + "\"}}";
            }
            else
            {
                diarize = "\"speakerName\": \"Speaker\"";
            }



            configuration_string = "{" +
                                                             "\"speechModel\": { \"language\" : \"" + language + "\", " +
                                                                                "\"features\": [\"voiceFeatures\"]" +
                                                                                "}," +
                                                              "\"vocabularies\":[{\"vocabularyName\": \"" + cusVocab + "\"}" +
                                                                                "]," +
                                                              "\"ingest\": {" +
                                                                                   //                                                                 "\"diarization\" : true," +
                                                                                   diarize +
                                                                           "} " +
                                                   //     "\"keywords\":{\"groups\":[\"Advertising Sales\", \"Subscriptions\", \"Buy\", \"Payments\", \"Missed Delivery\", \"Suspensions\", \"Retention\", \"Modify\", \"Account Changes\", \"Repeat Issues\", \"Transfers\", \"Negative Sentiment\", \"Positive Sentiment\" ]}, " +
                                                   //     "\"detections\":[{\"model\":\"PCI\"}]," +
                                                   //                                            "\"predictions\": [" +
                                                   //                                                             "{ \"model\" : \"c8b8c1f7-e663-4a05-a6b5-7e1109ee2947\" }" +
                                                   //                                                           "]" +
                                                   "}";

            metadata_string = "{" +
                                    "\"externalId\": \"" + yourCallId + "\"," +
                                    "\"extended\": { " +
                                                        "\"information\": { " +
                                                                            "\"validFraud\": 1 " +
                                                                         "}," +
                                                        "\"modelTargets\": { " +
                                                                            "\"fraud\": " + fraud +
                                                                         "}}}";


            request.AddParameter("configuration", configuration_string);
            request.AddParameter("metadata", metadata_string);
            //string filevalue = System.Convert.ToBase64String(file_get_byte_contents(file));
            //request.AddParameter("media", filevalue);
            string contentType = MimeMapping.GetMimeMapping(file2); 
            request.AddFile("media", file2, contentType);
            //            string body = "{configuration = " + configuration_string + "}";
            //            request.AddParameter("text/json", body, ParameterType.RequestBody);
            //            var resp = client.Execute(request);

            //request.AddParameter("filename", file2.Split(new char[] { '\\', '/' }).Last());
            //request.AddParameter("Content-Type","audio/mpeg");

            IRestResponse<MediaResponse> response = client.Execute<MediaResponse>(request);
            if (response.Data == null)
            {
                //do a pause and try again
                Thread.Sleep(1000);
                IRestResponse<MediaResponse> response2 = client.Execute<MediaResponse>(request);
                File.Delete(file2);
                if (response2.Data == null)
                {
                    return "Failed";
                }
                else
                {
                    return response2.Data.mediaId;
                }
            }
            else
            {
                File.Delete(file2);
                if (response.Data.status == "400")
                {
                    return "Failed";
                }
                else
                {
                    return response.Data.mediaId;
                }
            }

        }

        static byte[] file_get_byte_contents(string fileName)
        {
            byte[] sContents;
            if (fileName.ToLower().IndexOf("http:") > -1)
            {
                // URL 
                System.Net.WebClient wc = new System.Net.WebClient();
                sContents = wc.DownloadData(fileName);
            }
            else
            {
                // Get file size
                FileInfo fi = new FileInfo(fileName);

                // Disk
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                sContents = br.ReadBytes((int)fi.Length);
                br.Close();
                fs.Close();
            }

            return sContents;
        }


        static string GetMedia(string file, string cusVocab, NpgsqlConnection conf)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v2-beta");
            var request = new RestRequest(Method.POST);
            request.Resource = "/media";
            request.AddHeader("Authorization","Bearer "+token);
            request.AddFile("media", file);
            request.Timeout = 3000000;
            string configuration_string;
            string vocabs;
            string language;
            int channels = 1;
            string diarize = "";
            string left_speaker = "";
            string right_speaker = "";

            NpgsqlCommand sql = new NpgsqlCommand("select language, channels, left_channel_speaker, right_channel_speaker from cfg_transcript where id = '" + config.trans_config_id + "'", conf);

            using (var reader = sql.ExecuteReader())
            {
                if (!reader.Read())
                    throw new Exception("Something is wrong");
                language = reader.GetString(reader.GetOrdinal("language"));
                channels = reader.GetInt32(reader.GetOrdinal("channels"));
                left_speaker = reader.GetString(reader.GetOrdinal("left_channel_speaker"));
                right_speaker = reader.GetString(reader.GetOrdinal("right_channel_speaker"));
            }

            if (channels == 2)
            {
                diarize = "\"channels\": {\"left\": {\"speaker\": \"" + left_speaker + " \" },\"right\": {\"speaker\": \"" + right_speaker + "\"}}";
            }



            configuration_string = "{\"configuration\" : {" +
                                                             "\"executor\":\"v2\"," +
                                                             "\"language\":\""+language+"\"," +
                                                             "\"transcripts\": {\"engine\": \"premium\"," +
                                                                                "\"voiceFeatures\": \"true\"," +
              //                                                                  "\"formatNumbers\": \"true\"," +
                                                                                 "\"vocabularies\":[{\"name\": \""+cusVocab+"\"}]" +
                                                                                "}," +
                                                                "\"ingest\": {" +
               //                                                                 "\"diarization\" : true," +
                                                                                   diarize +
                                                                           "}, " +
               //     "\"keywords\":{\"groups\":[\"Advertising Sales\", \"Subscriptions\", \"Buy\", \"Payments\", \"Missed Delivery\", \"Suspensions\", \"Retention\", \"Modify\", \"Account Changes\", \"Repeat Issues\", \"Transfers\", \"Negative Sentiment\", \"Positive Sentiment\" ]}, " +
               //     "\"detections\":[{\"model\":\"PCI\"}]," +
               //                                            "\"predictions\": [" +
               //                                                             "{ \"model\" : \"c8b8c1f7-e663-4a05-a6b5-7e1109ee2947\" }" +
               //                                                           "]" +
                                                   "}}";

            request.AddParameter("configuration", configuration_string, ParameterType.RequestBody); 
            IRestResponse<MediaResponse> response = client.Execute<MediaResponse>(request);
            if (response.Data == null)
            {
                //do a pause and try again
                Thread.Sleep(1000);
                IRestResponse<MediaResponse> response2 = client.Execute<MediaResponse>(request);
                if (response2.Data == null)
                {
                    return "Failed";
                }
                else
                {
                    return response2.Data.mediaId;
                }
            }
            else
            {
                if (response.Data.status == "400")
                {
                    return "Failed";
                }
                else
                {
                    return response.Data.mediaId;
                }
            }

        }

        static void countFiles(string BaseMediaDir)
        {
            string[] dirEntries = Directory.GetDirectories(BaseMediaDir);
            string[] fileEntries;
            int i;
            foreach (string direct in dirEntries)
            {
                i = 0;
                fileEntries = Directory.GetFiles(Path.Combine(BaseMediaDir, direct));
                foreach (string file in fileEntries)
                {
                    var filename = Path.GetFileNameWithoutExtension(file);
                    var filenameExt = Path.GetFileName(file);
                    var fExt = Path.GetExtension(file);

                    if (fExt.ToLower() == ".wav")
                    {
                        i++; 
                    }
                    

                }
                Console.WriteLine(i.ToString() + " : " + direct);
            }
            string q = "erer";
        }


        static bool ReSendToVoiceBase(NpgsqlConnection lisaConn, string dir, string cusVocab, List<iSelect_meta> reflookup)
        {
            List<Task> tasks = new List<Task>();


            NpgsqlConnection fileconn = new NpgsqlConnection(config.dbConn);
            NpgsqlCommand selfile = new NpgsqlCommand("select rf.filename, rf.id, t.id as tId from dm_recording_file rf join transcript t on (t.recording_file_id = rf.id)" +
                                                        "where t.status = 'Failed' ", lisaConn);

            using (NpgsqlDataReader reader = selfile.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int rfId = reader.GetInt32(reader.GetOrdinal("id"));
                        string filename = reader.GetString(reader.GetOrdinal("filename"));
                        Guid tId = reader.GetGuid(reader.GetOrdinal("tId"));


                        NpgsqlConnection newconn = new NpgsqlConnection(config.dbConn);
                        newconn.Open();

                        string mediaId = null;
                        while (String.IsNullOrEmpty(mediaId))
                        {
                            mediaId = GetMedia(Path.Combine(dir, filename ), cusVocab, newconn);
                        }
                        DateTime CreateTime = DateTime.Now;
                        string status = "Submitted";
                        if (mediaId == "Failed")
                        {
                            status = "Failed";
                            mediaId = Guid.NewGuid().ToString();
                        }
                        NpgsqlCommand cmdInsert = new NpgsqlCommand("update transcript set ref_id = '"+mediaId+"', status = '"+status+"' where id = '"+tId.ToString()+"'", newconn);
                        cmdInsert.ExecuteNonQuery();

                        newconn.Close();

                    }
                }
            }

            //            Task.WaitAll(tasks.ToArray());
            return true;
        }

        static void CopyMP3Files(string BaseMediaDir, string outDir)
        {
            string[] dirEntries = Directory.GetDirectories(BaseMediaDir);
            string[] fileEntries;


            foreach (string dir in dirEntries)
            {
                string[] dir2Entries = Directory.GetDirectories(dir);

                foreach (string dir2 in dir2Entries)
                {
                    fileEntries = Directory.GetFiles(dir2);
                    foreach (string file in fileEntries)
                    {
                        var fExt = Path.GetExtension(file);
                        var filenameExt = Path.GetFileName(file);

                        if (fExt.ToLower() != ".mp3")
                        {
                            continue;
                        }

                        File.Copy(file, Path.Combine(outDir, filenameExt));

                    }
                }
            }
        }

        static bool SendToVoiceBase(NpgsqlConnection myConnection, string BaseInputDir, string BaseMediaDir, string DirDate, int noToProcess, string cusVocab)
        {
            List<Task> tasks = new List<Task>();

            string[] dirEntries = Directory.GetDirectories(BaseMediaDir);
            string[] fileEntries; 

            int i = 1;
            int iUser = 1;
            int iQueue = 1;
            string fraud;


            // Get last file id and userid

            foreach (string dir in dirEntries)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(dir);
                if (dirInfo.Name == "Fraudulent")
                {
                    fraud = "1";
                }
                else if (dirInfo.Name == "Truthful")
                {
                    fraud = "0";
                }
                else
                {
                    continue; // skip this directory
                }

                string[] dirEntries2 = Directory.GetDirectories(dir);

                foreach (string dir2 in dirEntries2)
                {


                    fileEntries = Directory.GetFiles(Path.Combine(dir2, "Sales"));
                    //foreach (string file in fileEntries)
                    Parallel.ForEach(fileEntries, new ParallelOptions { MaxDegreeOfParallelism = 8 }, (file) =>
                    {
                        NpgsqlConnection parCon = new NpgsqlConnection(config.dbConn);
                        parCon.Open();
                        i++;

                        var filename = Path.GetFileNameWithoutExtension(file);
                        var filenameExt = Path.GetFileName(file);
                        var fExt = Path.GetExtension(file);

                        if (fExt.ToLower() != ".mp3")
                        {
                            parCon.Close();
                            //continue;
                            return;
                        }

                        /*                        if (fExt.ToLower() == ".opus") // rename file to .ogg 
                                                {
                                                    File.Move(Path.Combine(dir, filenameExt), Path.Combine(dir, filename + ".ogg"));

                                                }*/

                        string dontcheck = "x";

                        NpgsqlCommand cmdSelect = new NpgsqlCommand("select * from dm_recording_file where filename = @filename", parCon);
                        cmdSelect.Parameters.AddWithValue("@filename", dontcheck + filenameExt);

                        using (NpgsqlDataReader reader = cmdSelect.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                            }
                            else
                            {
                                i++;
                                int file_id = i;

                                int user_id = 1;
                                int queue_id = 1;

                                // Check User

                                NpgsqlConnection newconn = new NpgsqlConnection(config.dbConn);
                                newconn.Open();

                                DateTime dateCall = new DateTime();
                                dateCall = DateTime.Now;
                                string transaction_id = filename;



                                NpgsqlCommand cmdInsert_interaction = new NpgsqlCommand("insert into dm_recording_interaction (transaction_id, user_id, date_time, queue_id) values ('" + transaction_id + "'," + user_id.ToString() + ",@date_time," + queue_id.ToString() + ") RETURNING id", newconn);
                                cmdInsert_interaction.Parameters.AddWithValue("@date_time", dateCall);
                                var int_id = cmdInsert_interaction.ExecuteScalar();

                                NpgsqlCommand cmdInsert_file = new NpgsqlCommand("insert into dm_recording_file ( recording_interaction_id, filename, location, length, status) values (@int_id, @filename,'" + dir2 + "',10,'active') RETURNING id", newconn);
                                cmdInsert_file.Parameters.AddWithValue("@filename", filenameExt);
                                cmdInsert_file.Parameters.AddWithValue("@int_id", int_id);
                                var file_pid = cmdInsert_file.ExecuteScalar();


                                string mediaId = null;
                                while (String.IsNullOrEmpty(mediaId))
                                {
                                    //                       mediaId = GetMedia(Path.Combine(dir2, "Sales", filenameExt), cusVocab, newconn);
                                    mediaId = GetMediaV3(Path.Combine(dir2, "Sales", filenameExt), cusVocab, newconn, fraud, file_pid.ToString());
                                }
                                DateTime CreateTime = DateTime.Now;
                                Guid trans_id = Guid.NewGuid();
                                string status = "Submitted";
                                if (mediaId == "Failed")
                                {
                                    status = "Failed";
                                    mediaId = Guid.NewGuid().ToString();
                                }
                                NpgsqlCommand cmdInsert = new NpgsqlCommand("insert into transcript (id, cfg_transcript_id, recording_file_id, ref_id, status) values ('" + trans_id + "','" + config.trans_config_id + "'," + file_pid + ",'" + mediaId + "','" + status + "' )", newconn);
                                cmdInsert.ExecuteNonQuery();

                                newconn.Close();
                                //                            }));
                            }
                        }
                        parCon.Close();
                    });
//                    }
                }
            }
//            Task.WaitAll(tasks.ToArray());
            return true;
        }

        static bool GetOutputFromVoiceBase(NpgsqlConnection myConnection, string outdir)
        {
            NpgsqlCommand cmdSelect = new NpgsqlCommand("select t.ref_id, rf.filename, rf.location from transcript t join dm_recording_file rf on (rf.id = t.recording_file_id) where t.status in ('running','Submitted','accepted')", myConnection);
            List<Task> tasks = new List<Task>();
            List<String> ref_ids = new List<string>();

            using (NpgsqlDataReader reader = cmdSelect.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string mediaId = reader.GetString(reader.GetOrdinal("ref_id"));
                        string filename = reader.GetString(reader.GetOrdinal("filename"));
                        string location = reader.GetString(reader.GetOrdinal("location"));

                        ref_ids.Add(mediaId);
                    }
                    Parallel.ForEach(ref_ids, new ParallelOptions { MaxDegreeOfParallelism = 8 }, (mediaId) =>
                    {

                        string JSON = GetTranscript(mediaId);
                        string destFile = "";

                            string status = "";
                            try
                            {

                                var json_serializer = new JavaScriptSerializer();
                                Media VBresponse = json_serializer.Deserialize<Media>(JSON);
                                status = VBresponse.media.status;
                                if (status=="finished")
                                {
                                destFile = System.IO.Path.Combine(outdir, mediaId + ".json");
                                File.WriteAllText(destFile, JSON);

                                } 
                           
                            }
                            catch (Exception)
                            {
                                return;
                            }

                            NpgsqlConnection newconn = new NpgsqlConnection(config.dbConn);
                            newconn.Open();
                            try
                            {
                                NpgsqlCommand update = new NpgsqlCommand("update transcript set source = @JSON, status = @status where ref_id = @mediaId", newconn);
                                NpgsqlParameter pJSON = new NpgsqlParameter();
                                pJSON.Value = JSON;
                                pJSON.ParameterName = "JSON";
                                pJSON.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Json;
                                update.Parameters.Add(pJSON);
                                update.Parameters.AddWithValue("mediaId", mediaId);
                                update.Parameters.AddWithValue("status", status);
                                update.ExecuteNonQuery();
                            }
                            catch (Exception)
                            {
                                //throw
                            }
                            newconn.Close();
                       });
                    }
            }
            return true;
        }



        static bool GetPlainTextVoiceBase(NpgsqlConnection myConnection, string outdir)
        {
            NpgsqlCommand cmdSelect = new NpgsqlCommand("select t.ref_id, rf.filename, rf.location from transcript t join dm_recording_file rf on (rf.id = t.recording_file_id) ", myConnection);
            List<Task> tasks = new List<Task>();
            List<String> ref_ids = new List<string>();

            using (NpgsqlDataReader reader = cmdSelect.ExecuteReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string mediaId = reader.GetString(reader.GetOrdinal("ref_id"));
                        string filename = reader.GetString(reader.GetOrdinal("filename"));
                        string location = reader.GetString(reader.GetOrdinal("location"));

                        ref_ids.Add(mediaId);
                    }
                    Parallel.ForEach(ref_ids, new ParallelOptions { MaxDegreeOfParallelism = 8 }, (mediaId) =>
                    {

                        string JSON = GetTranscriptPlain(mediaId);
                        string destFile = "";
                        destFile = System.IO.Path.Combine(outdir, mediaId + ".txt");
                        File.WriteAllText(destFile, JSON);

                    });
                }
            }
            return true;
        }

        static string GetTranscript(string mediaId)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v2-beta");
            var request = new RestRequest();
            request.Resource = "/media/" + mediaId;
            request.AddHeader("Authorization", "Bearer " + token);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        static string GetTranscriptPlain(string mediaId)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v2-beta");
            var request = new RestRequest();
            request.Resource = "/media/" + mediaId + "/transcripts/latest";
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Accept", "text/plain");
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        static string GetTranscriptPlainSrt(string mediaId)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v2-beta");
            var request = new RestRequest();
            request.Resource = "/media/" + mediaId + "/transcripts/latest";
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Accept", "text/srt");
            IRestResponse response = client.Execute(request);
            return response.Content;
        }


        static string SetVocab(NpgsqlConnection myConnection, string cusVocab)
        {
            NpgsqlCommand cmdSelectVocabs = new NpgsqlCommand("select vt.* from cfg_custom_vocab_term vt join cfg_custom_vocab v on vt.vocab_id = v.id " +
                                                                "where vocab_name = '" + cusVocab + "'", myConnection);
            NpgsqlDataReader readerVocabs = cmdSelectVocabs.ExecuteReader();
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v2-beta");
            var request = new RestRequest(Method.PUT);
            request.Resource = "/definitions/transcripts/vocabularies/" + cusVocab;
            request.AddHeader("Authorization", "Bearer " + token);

            if (readerVocabs.HasRows)
            {
                var vocablist = new List<string>();
                while (readerVocabs.Read())
                {
                    string sWord = readerVocabs.GetString(readerVocabs.GetOrdinal("words"));
                    string sTerm = sWord;
                    if (!readerVocabs.IsDBNull(readerVocabs.GetOrdinal("sounds_like")))
                    {
                        string sSoundsLike = readerVocabs.GetString(readerVocabs.GetOrdinal("sounds_like"));
                        sTerm = sTerm + ";" + sSoundsLike;
                    }
                    if (!readerVocabs.IsDBNull(readerVocabs.GetOrdinal("Weighting")))
                    {
                        if (readerVocabs.GetOrdinal("weighting") > 0)
                        {
                            sTerm = sTerm + ";" + readerVocabs.GetInt32(readerVocabs.GetOrdinal("weighting")).ToString();
                        }
                    }
                    vocablist.Add(sTerm.Trim());
                }
                var vocabulary = new Vocabs();
                vocabulary.name = cusVocab;
                vocabulary.terms = vocablist.ToArray();
                var sVocabContainer = new VocabContainer();
                sVocabContainer.vocabulary = vocabulary;
//                var serializer = new JavaScriptSerializer();
//                var vocabJSON = serializer(vocabulary);
//                request.AddParameter("vocabulary", vocabJSON, ParameterType.RequestBody);
//                request.AddParameter("configuration", "{\"configuration\" : {\"executor\":\"v2\"}", ParameterType.RequestBody);
                request.AddJsonBody(sVocabContainer);
                IRestResponse response = client.Execute(request);
                readerVocabs.Close();
                return response.Content;
            } else { return ""; }
        }

        static string SetVocabV3(NpgsqlConnection myConnection, string cusVocab)
        {
            NpgsqlCommand cmdSelectVocabs = new NpgsqlCommand("select vt.* from cfg_custom_vocab_term vt join cfg_custom_vocab v on vt.vocab_id = v.id " +
                                                                "where vocab_name = '" + cusVocab + "'", myConnection);
            NpgsqlDataReader readerVocabs = cmdSelectVocabs.ExecuteReader();
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v3");
            var request = new RestRequest(Method.PUT);
            request.Resource = "/definition/vocabularies/" + cusVocab;
            request.AddHeader("Authorization", "Bearer " + token);

            if (readerVocabs.HasRows)
            {
                var vocablist = new List<Terms>();
                while (readerVocabs.Read())
                {
                    var term = new Terms();
                    term.term = readerVocabs.GetString(readerVocabs.GetOrdinal("words"));
                    if (!readerVocabs.IsDBNull(readerVocabs.GetOrdinal("sounds_like")))
                    {
                        string sSoundsLike = readerVocabs.GetString(readerVocabs.GetOrdinal("sounds_like"));
                    }
                    if (!readerVocabs.IsDBNull(readerVocabs.GetOrdinal("Weighting")))
                    {
                        if (readerVocabs.GetOrdinal("weighting") > 0)
                        {
                            term.weight =  readerVocabs.GetInt32(readerVocabs.GetOrdinal("weighting"));
                        }
                    }
                    vocablist.Add(term);
                }

                var vocabulary = new VocabsV3();
                vocabulary.vocabularyName = cusVocab;
                vocabulary.terms = vocablist.ToArray();
                request.AddJsonBody(vocabulary);
                IRestResponse response = client.Execute(request);
                readerVocabs.Close();
                return response.Content;
            }
            else { return ""; }
        }


        static string SetKeywordGroup()
        {
            var client = new RestClient();
            client.BaseUrl = new Uri("https://apis.voicebase.com/v2-beta");
            var request = new RestRequest(Method.PUT);

            request.Resource = "/definitions/keywords/groups/GroupName";
            request.AddHeader("Authorization", "Bearer " + token);

            var keywordlist = new List<string>();
            //   Add keywords to list
            keywordlist.Add("word 1");
            keywordlist.Add("word 2");


            var keywords = new KeywordsSet();
            keywords.name = "GroupName";
            keywords.keywords = keywordlist.ToArray();

            var keywordgroup = new KeywordGroup();

//            keywordgroup.group = keywords;

            request.AddJsonBody(keywordgroup);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

    }
}